function displayList(array, parent = document.body) {
    const list = document.createElement("ul");
    parent.appendChild(list);
    for (let i = 0; i < array.length; i++) {
      const listItem = document.createElement("li");
      listItem.textContent = array[i];
      list.appendChild(listItem);
    }
  }
  const myArray1 = ["hello", "world", "Kiev", "Kharkiv", "Odessa", "Lviv"];
displayList(myArray1);

const myArray2 = ["1", "2", "3", "sea", "user", 23];
const parentElement = document.getElementById("myListContainer");
displayList(myArray2, parentElement);
  
console.log (myArray1,myArray2);